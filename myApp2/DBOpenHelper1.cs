﻿using System;
using Android.Database.Sqlite;
using Android.Content;
namespace myApp2
{
	public class DBOpenHelper1 : SQLiteOpenHelper
	{
		// Database Version
		private static int DATABASE_VERSION = 1;
		// Database Name
		private static String DATABASE_NAME = "TestDatabaseV6.db";

		// Tables name
		private static String TABLE_EVENT = "event";
		private static String TABLE_CATEGORY = "category";
		private static String TABLE_SETTING = "setting";

		// Event Column Names
		private static String EVENT_ID = "eventID";
		private static String EVENT_NAME = "name";
		private static String EVENT_DESCRIPTION = "description";
		private static String EVENT_STARTDATE = "startdate";
		private static String EVENT_STARTTIME = "starttime";
		private static String EVENT_UPORPAST = "uporpast";
		private static String EVENT_CATEGORY = "categoryID";
		private static String EVENT_ALARM_ID = "alarmID";
		private static String EVENT_PERTIME = "pertime";
		private static String EVENT_FREQUENCY = "frequency";

		// Category Column Names
		private static String CATEGORY_ID = "categoryID";
		private static String CATEGORY_NAME = "categoryname";

		// Setting Column Names
		private static String SETTING_ID = "settingID";
		private static String SETTING_PERTIME = "pertime";
		private static String SETTING_FREQUENCY = "frequency";
		private static String SETTING_ALARM = "alarm";

		// Create Statements
		private static String CREATE_TABLE_EVENT = "create table "
				+ TABLE_EVENT + "(" + EVENT_ID + " integer primary key autoincrement, "
				+ EVENT_NAME + " text, " + EVENT_DESCRIPTION + " text, "
				+ EVENT_PERTIME + " text, " + EVENT_FREQUENCY + " integer, "
				+ EVENT_STARTDATE + " text, " + EVENT_STARTTIME + " text, "
				+ EVENT_UPORPAST + " integer, " + EVENT_ALARM_ID + " text, " + EVENT_CATEGORY + " integer, " +
				"CONSTRAINT FK_CATEGORY_ID FOREIGN KEY (categoryID) REFERENCES category (category_ID)" + " );";

		private static String CREATE_TABLE_CATEGORY = "create table "
			+ TABLE_CATEGORY + "(" + CATEGORY_ID + " integer primary key autoincrement, "
			+ CATEGORY_NAME + " text);";

		private static String CREATE_TABLE_SETTING = "create table "
			+ TABLE_SETTING + "(" + SETTING_ID + " integer primary key autoincrement, "
			+ SETTING_PERTIME + " text, " + SETTING_FREQUENCY + " text, "
			+ SETTING_ALARM + " text);";

		public DBOpenHelper1(Context context)
				: base(context, DATABASE_NAME, null, 1)
		{
		}
		public override void OnCreate(SQLiteDatabase db)
		{
			db.ExecSQL(CREATE_TABLE_SETTING);
			db.ExecSQL(CREATE_TABLE_CATEGORY);
			db.ExecSQL(CREATE_TABLE_EVENT);
		}

		public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
			// Drop table if existed
			db.ExecSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
			db.ExecSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
			db.ExecSQL("DROP TABLE IF EXISTS " + TABLE_SETTING);

			// Creating new tables
			OnCreate(db);
		}

	}
}

