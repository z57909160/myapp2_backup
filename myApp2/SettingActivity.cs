﻿using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;

namespace myApp2
{
	[Activity(Label = "SettingActivity", ParentActivity = typeof(MainActivity))]
	[MetaData("android.support.PARENT_ACTIVITY", Value = "com.companyname.myapp2.MainActivity")]
	public class SettingActivity : ActionBarActivity
	{
		private Android.Media.MediaPlayer mediaPlayer;
		private Vibrator vibrator;

		//theme in use
		//0:original 
		//1: natural
		//2:dark
		int theme;
		int language;
		Locale locale;
		string languageToLoad;

		Spinner spnTheme;
		Spinner spnLanguage;
		Spinner spn_alarm;

		private string str_alarm;
		Button btnSave;
		Button btnDefault;
		Button btnPlay;
		Button btn_vib;
		Button btn_play;

		DBSettingHelper dbSettingHelper;
		SettingClass setting = new SettingClass();

		TextView textTitle;

		//for backup and restore
		//string accessToken = null;
		//static private String APP_KEY = "8qgsq3aypf1jgb6";
		//static private String APP_SECRET = "jygf6u9rtwatsvf";
		//private DropboxAPI<AndroidAuthSession> mDBApi;
		public string uniqueID = null;
		public bool ifExist = false;

		private static string DATABASE_NAME = "TestDatabaseV6.db";
		public static string PACKAGE_NAME = "com.companyname.myapp2";
		public static string DB_PATH = "/data" + Android.OS.Environment.DataDirectory.AbsolutePath + "/" + PACKAGE_NAME;
		public static string DB_LOCATION = DB_PATH + "/databases/" + DATABASE_NAME;
		//AndroidAuthSession session;
		string revNumber = "0"; //give a whatever value to initialize, better not to be null
		bool isSameUpload = false;

		//initialize preference
		ISharedPreferences settings;
		ISharedPreferencesEditor editor1;
		ISharedPreferences preferences;
		string vibrationStatus;
		System.Threading.Thread th_play;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait; //set to portrait

			base.OnCreate(savedInstanceState);

			dbSettingHelper = new DBSettingHelper(this);

			// Get a SharedPreferences and its values
			preferences = GetSharedPreferences(MainActivity.PREFS_NAME, 0);
			theme = preferences.GetInt("theme", 0);
			language = preferences.GetInt("language", 0);

			// Set the language for the activity
			if (language == 0)
			{
				languageToLoad = "en";

			}
			else if (language == 1)
			{
				languageToLoad = "zh";
			}
			else if (language == 2)
			{
				languageToLoad = "vi";
			}
			else if (language == 3)
			{
				languageToLoad = "in";
			}
			else {
				languageToLoad = "en";
			}

			locale = new Locale(languageToLoad);
			Locale.Default = locale;
			Configuration config = new Configuration();
			config.Locale = locale;
			BaseContext.Resources.UpdateConfiguration(config,
					BaseContext.Resources.DisplayMetrics);
			SupportActionBar.Title = GetText(Resource.String.title_activity_settings);


			SetContentView(Resource.Layout.activity_settings);
			// Set our view from the "main" layout resource
			// Set the theme to user's choice or default
			// Initialise widgets
			btn_vib = (Button)FindViewById<Button>(Resource.Id.btn_vibration);
			spn_alarm = (Spinner)FindViewById<Spinner>(Resource.Id.spnAlarm);
			spnTheme = (Spinner)FindViewById<Spinner>(Resource.Id.spn_theme);
			spnLanguage = (Spinner)FindViewById<Spinner>(Resource.Id.spn_language);
			btnSave = (Button)FindViewById<Button>(Resource.Id.button4);
			btnDefault = (Button)FindViewById<Button>(Resource.Id.button5);
			btnPlay = (Button)FindViewById<Button>(Resource.Id.btn_play);
			textTitle = (TextView)FindViewById(Resource.Id.settingspage_title);
			btn_play = (Button)FindViewById<Button>(Resource.Id.btn_play);


			if (theme == 0)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);
				spnTheme.SetSelection(0);
			}
			else if (theme == 1)
			{

				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.natural_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.natural_background_color));
				textTitle.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				btnSave.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnDefault.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnPlay.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));

				spnTheme.SetSelection(1);

			}
			else if (theme == 2)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.dark_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.dark_background_color));
				textTitle.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				btnSave.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnDefault.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnPlay.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));

				spnTheme.SetSelection(2);
			}
			else {
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);
				spnTheme.SetSelection(0);
			}

			mediaPlayer = new MediaPlayer();

			//get preference of setting
			settings = GetSharedPreferences("my_settings", 0);
			editor1 = settings.Edit(); //for vibrator

			//preference of vibration
			vibrationStatus = settings.GetString("vibration", "on");



			//set button style according to theme currently in use
			if (vibrationStatus.Equals("on"))
			{
				btn_vib.Text = GetString(Resource.String.btn_vibration_off);
				btn_vib.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.reddesignbutton));
			}
			else {
				btn_vib.Text = GetString(Resource.String.btn_vibration_on);
				if (theme == 0)
				{
					btn_vib.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.designbutton));
				}
				else if (theme == 1)
				{
					btn_vib.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.naturaldesignbutton));
				}
				else if (theme == 2)
				{
					btn_vib.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.darkdesignbutton));
				}
				else {
					btn_vib.SetBackgroundDrawable(Resources.GetDrawable(Resource.Color.main_color));
				}
			}
			// Set the selection of spnLanguage dropdownlist to default or user's selection
			if (language == 0)
			{
				spnLanguage.SetSelection(0);
			}
			else if (language == 1)
			{
				spnLanguage.SetSelection(1);
			}
			else if (language == 2)
			{
				spnLanguage.SetSelection(2);
			}
			else if (language == 3)
			{
				spnLanguage.SetSelection(3);
			}
			else {
				spnLanguage.SetSelection(0);
			}

			//initialization for backup and restore
			//AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
			//session = new AndroidAuthSession(appKeys);
			//mDBApi = new DropboxAPI<AndroidAuthSession>(session);
			//accessToken = "Hts9VBG4XhAAAAAAAAAAH4SbkVa5I-e6hr9FO7xAvB6HivR5NdRdAloBGhENeLYK";
			//uniqueID = getUniquePsuedoID(); //get the unique ID as the user's ID of this device


			this.SupportActionBar.SetDisplayShowHomeEnabled(true); // Support a back button to previous Activity
			this.SupportActionBar.SetHomeButtonEnabled(true);

			if (dbSettingHelper.getCount() == 0)  //if it's a first-time run
			{
				spn_alarm.SetSelection(0, true);
				setting.setAlarm("default_music");
				dbSettingHelper.addDefaultSetting(setting);

			}

			else {  // otherwise, load setting from database
				setting = dbSettingHelper.getSetting();
				int index = 0;
				if (setting.getAlarm().Equals("default_music")) { index = 0; }
				else if (setting.getAlarm().Equals("wake_up")) { index = 1; }
				else if (setting.getAlarm().Equals("exciting")) { index = 2; }
				else if (setting.getAlarm().Equals("lyrical")) { index = 3; }
				spn_alarm.SetSelection(index, true);
			}

			btnSave.Click += delegate
			{
				save();
			};
			btnPlay.Click += delegate
			{
				play();
				Console.WriteLine("****");
			};

			btnDefault.Click += delegate
			{
				spn_alarm.SetSelection(0, true);
				spnTheme.SetSelection(0, true);
				spnLanguage.SetSelection(0, true);
			};

			btn_vib.Click += delegate
			{
				Vibration();
			};

		}

		public void save()
		{

			//save alarm music preference to database
			if (spn_alarm.SelectedItemId == 0)
			{
				str_alarm = "default_music";
			}
			else if (spn_alarm.SelectedItemId == 1)
			{
				str_alarm = "wake_up";
			}
			else if (spn_alarm.SelectedItemId == 2)
			{
				str_alarm = "exciting";
			}
			else if (spn_alarm.SelectedItemId == 3)
			{
				str_alarm = "lyrical";
			}

			//add to database
			setting.setAlarm(str_alarm);
			dbSettingHelper.save(setting);
			Toast.MakeText(this, GetString(Resource.String.savesuccessfully_text),
					ToastLength.Short).Show();

			// Create a SharedPreferences.Editor to edit preferences
			ISharedPreferencesEditor editor = preferences.Edit();

			if (spnTheme.SelectedItemId == 0)
			{
				editor.Remove("theme");
				editor.PutInt("theme", 0);
			}
			else if (spnTheme.SelectedItemId == 1)
			{
				editor.Remove("theme");
				editor.PutInt("theme", 1);
			}
			else if (spnTheme.SelectedItemId == 2)
			{
				editor.Remove("theme");
				editor.PutInt("theme", 2);
			}

			//set language to preference
			if (spnLanguage.SelectedItemId == 0)
			{
				editor.Remove("language");
				editor.PutInt("language", 0);
			}
			else if (spnLanguage.SelectedItemId == 1)
			{
				editor.Remove("language");
				editor.PutInt("language", 1);
			}
			else if (spnLanguage.SelectedItemId == 2)
			{
				editor.Remove("language");
				editor.PutInt("language", 2);
			}
			else if (spnLanguage.SelectedItemId == 3)
			{
				editor.Remove("language");
				editor.PutInt("language", 3);
			}
			editor.Commit(); // Commit editor
			editor1.Commit(); //also commit
			Console.WriteLine("***" + preferences.GetInt("theme", 0));
		}

		//***********************vibration******************
		public void Vibration()
		{ //vibration function
			Console.WriteLine("***vibstart");
			if (btn_vib.Text.Equals(GetString(Resource.String.btn_vibration_on)))
			{
				btn_vib.Text = GetString(Resource.String.btn_vibration_off); //now vibration is on
				btn_vib.SetBackgroundDrawable(GetDrawable(Resource.Drawable.reddesignbutton));
				Toast.MakeText(this, GetString(Resource.String.vibration_is_on), ToastLength.Short).Show();
				//give a vibration
				vibrator = (Vibrator)GetSystemService(Context.VibratorService);
				long[] pattern = { 100, 400, 100, 400 };   // off on off on
				vibrator.Vibrate(pattern, -1); // 2 means vibrate 2 times
				editor1.PutString("vibration", "on");
			}
			else
			{
				btn_vib.Text = GetText(Resource.String.btn_vibration_on);//now vibration is off
																		 //set color
				if (theme == 0)
				{
					btn_vib.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				}
				else if (theme == 1)
				{
					btn_vib.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				}
				else if (theme == 2)
				{
					btn_vib.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				}
				else {
					btn_vib.SetBackgroundDrawable(GetDrawable(Resource.Color.main_color));
				}
				editor1.PutString("vibration", "off");
				Toast.MakeText(this, GetString(Resource.String.vibration_is_off), ToastLength.Short).Show();

			}
		}



		//************************play function*****************************


		public void play()
		{
			if (th_play != null)
			{
				if (th_play.IsAlive) th_play.Interrupt();
			}
			th_play = new System.Threading.Thread(new ThreadStart(run_play_ui));
			th_play.Start();
			Console.WriteLine("****Thread Start");
		}
		private void run_play_ui() { RunOnUiThread(run_play); Console.WriteLine("****Thread run on ui"); }
		public void run_play()
		{
			if (btn_play.Text.Equals(GetString(Resource.String.btn_testMusic)))
			{
				if (mediaPlayer.IsPlaying == true) { mediaPlayer.Stop(); }
				if (spn_alarm.SelectedItemId == 0)
				{
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.default_music);
				}
				else if (spn_alarm.SelectedItemId == 2)
				{
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.exciting);
				}
				else if (spn_alarm.SelectedItemId == 3)
				{
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.lyrical);
				}
				else if (spn_alarm.SelectedItemId == 1)
				{
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.wake_up);
				}
				mediaPlayer.Looping = false;
				mediaPlayer.Start();
				btn_play.Text = GetString(Resource.String.btn_vibration_off); //now is playing
				btn_play.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.reddesignbutton));
				spn_alarm.Enabled = false;
			}
			else {
				if (mediaPlayer.IsPlaying == true) { mediaPlayer.Stop(); }
				btn_play.Text = GetString(Resource.String.btn_testMusic);
				//set color
				if (theme == 0)
				{
					btn_play.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.designbutton));
				}
				else if (theme == 1)
				{
					btn_play.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.naturaldesignbutton));
				}
				else if (theme == 2)
				{
					btn_play.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.darkdesignbutton));
				}
				else {
					btn_vib.SetBackgroundDrawable(Resources.GetDrawable(Resource.Color.main_color));
				}
				spn_alarm.Enabled = true;
			}
			Console.WriteLine("****Thread executed");
		}


	}


}

