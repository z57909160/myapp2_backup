﻿using System;
using Android.Content;
using Android.Database;
using Android.Database.Sqlite;

namespace myApp2
{
	public class DBEventHelper : MainActivity
	{
		DBOpenHelper1 dbOpenHelper;
		public DBEventHelper(Context context)
		{
			this.dbOpenHelper = new DBOpenHelper1(context);
		}
		public void addEvent(EventClass e)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;
			ContentValues cv = new ContentValues();
			//Put content value
			cv.Put("name", e.getName());
			cv.Put("description", e.getDescription());
			cv.Put("startdate", e.getStartDate());
			cv.Put("starttime", e.getStartTime());
			cv.Put("uporpast", e.getStatus());
			cv.Put("alarmID", e.getAlarmID());
			cv.Put("categoryID", e.getCategoryID());
			cv.Put("pertime", e.getPretime());
			cv.Put("frequency", e.getFrequency());

			/* insert data */
			db.Insert("event", null, cv);
			db.Close();

		}

		//get an e according to given alarmID
		public EventClass gete(String AlarmID)
		{ //Note: alarmID is a String type data
			SQLiteDatabase db = dbOpenHelper.ReadableDatabase;
			ICursor cursor = db.RawQuery("select * from event where alarmID=?", new String[] { AlarmID });
			if (cursor.MoveToFirst())
			{
				Boolean a;
				if (cursor.GetInt(cursor.GetColumnIndex("uporpast")) == 0) { a = false; }
				else {
					a = true;
				}
				EventClass e = new EventClass();
				e.setID(cursor.GetInt(cursor.GetColumnIndex("eventID")));

				e.setName(cursor.GetString(cursor.GetColumnIndex("name")));

				e.setDescription(cursor.GetString(cursor.GetColumnIndex("description")));

				e.setStartDate(cursor.GetString(cursor.GetColumnIndex("startdate")));

				e.setStartTime(cursor.GetString(cursor.GetColumnIndex("starttime")));

				e.setStatus(a);

				e.setCategoryID(cursor.GetInt(cursor.GetColumnIndex("categoryID")));

				e.setAlarmID(cursor.GetString(cursor.GetColumnIndex("alarmID")));

				e.setPretime(cursor.GetInt(cursor.GetColumnIndex("pertime")));

				e.setFrequency(cursor.GetInt(cursor.GetColumnIndex("frequency")));
				return e; //return the e
			}
			cursor.Close();
			return null;
		}

		//modify a e by
		//1.create an e     --EventClass e
		//2.set the e according to the e's alarmID which is unique ID --e.gete(alarmID)
		//3.change the columns values  ----e.set***();
		//4.Updatee(e)
		public void Update(EventClass e)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;
			ContentValues cv = new ContentValues();
			cv.Put("name", e.getName());
			cv.Put("description", e.getDescription());
			cv.Put("startdate", e.getStartDate());
			cv.Put("starttime", e.getStartTime());
			cv.Put("uporpast", e.getStatus());
			cv.Put("alarmID", e.getAlarmID());
			cv.Put("categoryID", e.getCategoryID());
			cv.Put("pertime", e.getPretime());
			cv.Put("frequency", e.getFrequency());
			db.Update("event", cv, "alarmID=" + e.getAlarmID(), null);
			db.Close();
		}


		public void UpdateEventByeventID(EventClass e)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;
			ContentValues cv = new ContentValues();
			cv.Put("name", e.getName());
			cv.Put("description", e.getDescription());
			cv.Put("startdate", e.getStartDate());
			cv.Put("starttime", e.getStartTime());
			//cv.Put("uporpast", e.getStatus());
			cv.Put("alarmID", e.getAlarmID());
			cv.Put("categoryID", e.getCategoryID());
			cv.Put("pertime", e.getPretime());
			cv.Put("frequency", e.getFrequency());
			db.Update("event", cv, "eventID=" + e.getID(), null);
			db.Close();
		}

		//similar to Update e, but requires insert all arguments
		public void modifyEvent(int id, String name, String description, String startdate, String starttime, int uporpast, int categoryid)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;

			ContentValues args = new ContentValues();
			args.Put("name", name);
			args.Put("description", description);
			args.Put("startdate", startdate);
			args.Put("starttime", starttime);
			args.Put("uporpast", uporpast);
			args.Put("categoryid", categoryid);

			db.Update("event", args, "eventID=" + id, null);
			db.Close();
		}

		//Note: all autoincrement ID start from 1, NOT zero
		public void deleteEvent(int index)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;
			db.ExecSQL("DELETE FROM event WHERE eventID = " + index.ToString());
			db.Close();
		}

		//getCursor from database
		public ICursor getCursor()
		{
			SQLiteDatabase db = dbOpenHelper.ReadableDatabase;
			ICursor cur = null;
			cur = db.Query("event", new String[] { "eventID" + " as _id", "name", "description", "starttime", "startdate", "uporpast", "categoryID" }, null, null, null, null, null);
			return cur;
		}
		/**
		 * @param
		 */

		//tai
		public ICursor getCursoronEventID(int eID)
		{
			SQLiteDatabase db = dbOpenHelper.ReadableDatabase;
			ICursor cur = null;
			cur = db.RawQuery("select name,description,startdate,starttime,categoryID,alarmID,pertime,frequency,uporpast from event where eventID=" + eID, null);
			cur.MoveToFirst();
			return cur;
		}
	}
}

