﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Database;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Speech;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Util;

namespace myApp2
{
	[Activity(Label = "DetailsActivity", ParentActivity = typeof(MainActivity))]
	[MetaData("android.support.PARENT_ACTIVITY", Value = "com.companyname.myapp2.MainActivity")]
	public class DetailsActivity : ActionBarActivity
	{

		int year_startE, month_startE, day_startE;
		int hour_startE, minute_startE;
		static int STARTTIME_DIALOG_ID = 1;
		DBEventHelper dbEventHelper;

		EventClass eventE = new EventClass();

		String evidDetail;

		ICursor cursor = null;

		TextView textLabelTitle;
		TextView textLabelDescription;
		TextView textLabelDate;
		TextView textLabelCategory;
		TextView txtTitleE;
		TextView txtDesE;
		TextView preTimeText;
		TextView frequencyText;
		Button btnStartTimeE;
		Button btnStartDateE;
		Spinner spn_category;
		Spinner spn_pretimeE;
		Spinner spn_frequencyE;
		Button btnEditE;
		Button btnSaveE;
		Button btnResetE;
		Button btnRemoveE;
		Button voiceBtn;
		Button voiceDescBtn;

		String[] dateE;
		String[] timeE;

		String alarmID;
		public static Java.Util.TimeZone tz = Java.Util.TimeZone.GetTimeZone("Australia/Sydney");
		ISharedPreferences preferences;

		const int REQ_CODE_SPEECH_INPUT = 1;
		const int REQ_CODE_SPEECH_DESCRIPTION = 2;

		int theme;
		int language;
		Locale locale;
		string languageToLoad;

		private AlarmManager am1 = null; // created the alarmManager
		private AlarmManager am2 = null; // created the alarmManager
		Calendar now;
		Calendar selectedTimeE;

		//get preference
		ISharedPreferences settings;
		ISharedPreferencesEditor editor;

		int daysOfPretimeE; //how nay days pf pretime setting
		int hoursOfFrequencyE;//how nay days pf frequency setting


		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait; //set to portrait

			//initialise shared preferences
			settings = GetSharedPreferences("my_settings", 0);
			editor = settings.Edit(); //for vibrator

			preferences = GetSharedPreferences(MainActivity.PREFS_NAME, 0);
			theme = preferences.GetInt("theme", 0);
			language = preferences.GetInt("language", 0);


			base.OnCreate(savedInstanceState);

			// Set the language for the activity
			if (language == 0)
			{
				languageToLoad = "en";

			}
			else if (language == 1)
			{
				languageToLoad = "zh";
			}
			else if (language == 2)
			{
				languageToLoad = "vi";
			}
			else if (language == 3)
			{
				languageToLoad = "in";
			}
			else {
				languageToLoad = "en";
			}

			locale = new Locale(languageToLoad);
			Locale.Default = locale;
			Configuration config = new Configuration();
			config.Locale = locale;
			BaseContext.Resources.UpdateConfiguration(config,
					BaseContext.Resources.DisplayMetrics);
			SupportActionBar.Title = GetText(Resource.String.title_activity_details);

			SetContentView(Resource.Layout.activity_details);
			dbEventHelper = new DBEventHelper(this);

			//*******initialise widgets************
			// Initalise widgets
			textLabelTitle = (TextView)FindViewById(Resource.Id.lblTit);
			textLabelDescription = (TextView)FindViewById(Resource.Id.lblDes);
			textLabelDate = (TextView)FindViewById(Resource.Id.textView2);
			textLabelCategory = (TextView)FindViewById(Resource.Id.textView8);
			txtTitleE = (TextView)FindViewById(Resource.Id.txtTitleE);
			txtDesE = (TextView)FindViewById(Resource.Id.txtDesE);
			btnStartTimeE = (Button)FindViewById(Resource.Id.btnStartTimeE);
			btnStartDateE = (Button)FindViewById(Resource.Id.btnStartDateE);
			spn_category = (Spinner)FindViewById(Resource.Id.spnCategoryE);
			spn_frequencyE = (Spinner)FindViewById(Resource.Id.spnFrequencyE);
			spn_pretimeE = (Spinner)FindViewById(Resource.Id.spnPreTimeE);
			btnEditE = (Button)FindViewById(Resource.Id.btnEditE);
			btnSaveE = (Button)FindViewById(Resource.Id.btnSaveE);
			btnResetE = (Button)FindViewById(Resource.Id.btnResetE);
			btnRemoveE = (Button)FindViewById(Resource.Id.btnRemove);
			voiceBtn = (Button)FindViewById(Resource.Id.btnVoiceE);
			voiceDescBtn = (Button)FindViewById(Resource.Id.btnVoiceDescriptionE);
			preTimeText = (TextView)FindViewById(Resource.Id.textView);
			frequencyText = (TextView)FindViewById(Resource.Id.textView3);

			//***theme
			this.SupportActionBar.SetDisplayShowHomeEnabled(true); // Support a back button to previous Activity
			this.SupportActionBar.SetHomeButtonEnabled(true);
			if (theme == 0)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);

				btnStartDateE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnStartTimeE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnEditE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnSaveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnResetE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnRemoveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				voiceBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				voiceDescBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
			}
			else if (theme == 1)
			{

				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.natural_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.natural_background_color));

				textLabelTitle.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				textLabelDescription.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				textLabelDate.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				textLabelCategory.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				preTimeText.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				frequencyText.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));

				btnStartDateE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnStartTimeE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnEditE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnSaveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnResetE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnRemoveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				voiceBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				voiceDescBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));

			}
			else if (theme == 2)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.dark_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.dark_background_color));

				textLabelTitle.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				textLabelTitle.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				textLabelDescription.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				textLabelDate.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				textLabelCategory.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				preTimeText.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				frequencyText.SetTextColor(Resources.GetColor(Resource.Color.white_color));

				btnStartDateE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnStartTimeE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnEditE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnSaveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnResetE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnRemoveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				voiceBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				voiceDescBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
			}
			else {
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);

				btnStartDateE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnStartTimeE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnEditE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnSaveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnResetE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				btnRemoveE.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				voiceBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
				voiceDescBtn.SetBackgroundDrawable(GetDrawable(Resource.Drawable.designbutton));
			}


			//Date dialog listener
			btnStartDateE.Click += delegate
			{
				DateOnClick();
			};
			//Time Dialog
			btnStartTimeE.Click += (o, e) => ShowDialog(STARTTIME_DIALOG_ID);

			btnEditE.Click += delegate
			{
				SetControls(true);
			};

			btnSaveE.Click += delegate
			{
				prepareSave();
			};

			btnRemoveE.Click += delegate
			{
				btnRemove();
			};

			btnResetE.Click += delegate
			{
				btnResetClick();
			};
			voiceBtn.Click += delegate
			{
				promptSpeechInput();
			};
			voiceDescBtn.Click += delegate
			{
				promptSpeechInputDescription();
			};

			//get the eventid on the main page
			evidDetail = Intent.GetStringExtra("eventid");
			//check the value
			if (evidDetail == "")
			{
				//return back to the list recipe if null value
				Intent intent = new Intent(this, typeof(MainActivity));
				StartActivity(intent);
			}
			else {
				cursor = dbEventHelper.getCursoronEventID(int.Parse(evidDetail));
				do
				{
					//set the values from the database to the controllers and the object event
					//the eventE use for reset the value of the controllers
					txtTitleE.Text = cursor.GetString(cursor.GetColumnIndex("name"));
					eventE.setName(cursor.GetString(cursor.GetColumnIndex("name")));
					txtDesE.Text = cursor.GetString(cursor.GetColumnIndex("description"));
					eventE.setDescription(cursor.GetString(cursor.GetColumnIndex("description")));

					timeE = cursor.GetString(cursor.GetColumnIndex("starttime")).Split(':');

					btnStartTimeE.Text = timeE[0] + ":" + timeE[1];
					eventE.setStartTime(timeE[0] + ":" + timeE[1]);
					hour_startE = int.Parse(timeE[0]);
					minute_startE = int.Parse(timeE[1]);

					//check is where the event come from? in upcoming list or past list
					if (cursor.GetInt(cursor.GetColumnIndex("uporpast")) == 0)
					{
						btnEditE.Visibility = ViewStates.Invisible;
					}

					dateE = cursor.GetString(cursor.GetColumnIndex("startdate")).Split('/');

					btnStartDateE.Text = dateE[0] + "/" + dateE[1] + "/" + dateE[2];
					eventE.setStartDate(dateE[0] + "/" + dateE[1] + "/" + dateE[2]);
					year_startE = int.Parse(dateE[0]);
					month_startE = int.Parse(dateE[1]);
					day_startE = int.Parse(dateE[2]);

					spn_category.SetSelection(cursor.GetInt(cursor.GetColumnIndex("categoryID")));
					eventE.setCategoryID(cursor.GetInt(cursor.GetColumnIndex("categoryID")));
					spn_frequencyE.SetSelection(cursor.GetInt(cursor.GetColumnIndex("frequency")));
					eventE.setFrequency(cursor.GetInt(cursor.GetColumnIndex("frequency")));
					spn_pretimeE.SetSelection(cursor.GetInt(cursor.GetColumnIndex("pertime")));
					eventE.setPretime(cursor.GetInt(cursor.GetColumnIndex("pertime")));

					alarmID = cursor.GetString(cursor.GetColumnIndex("alarmID"));
					eventE.setAlarmID(cursor.GetString(cursor.GetColumnIndex("alarmID")));
					eventE.setID(int.Parse(evidDetail));
				} while (cursor.MoveToNext());

				cursor.Close();
				SetControls(false);


			}
		}

		private void TimePickerCallback(object sender, TimePickerDialog.TimeSetEventArgs e)
		{
			hour_startE = e.HourOfDay;
			minute_startE = e.Minute;
			string time = string.Format("{0}:{1}", hour_startE, minute_startE.ToString().PadLeft(2, '0'));
			btnStartTimeE.Text = time;
		}
		protected override Dialog OnCreateDialog(int id)
		{
			// Get the current time
			hour_startE = DateTime.Now.Hour;
			minute_startE = DateTime.Now.Minute;

			if (id == STARTTIME_DIALOG_ID)
				return new TimePickerDialog(this, TimePickerCallback, hour_startE, minute_startE, false);

			return null;
		}

		public void DateOnClick()
		{
			DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
			 {
				 year_startE = time.Year;
				 month_startE = time.Month;
				 day_startE = time.Day;
				 btnStartDateE.Text = btnStartTimeE.Text = year_startE + "/" + month_startE + "/" + day_startE;
			 });
			frag.Show(FragmentManager, DatePickerFragment.TAG);
		}

		//set all controllers become the beginning values
		public void btnResetClick()
		{
			txtTitleE.Text = eventE.getName();
			txtDesE.Text = eventE.getDescription();

			btnStartTimeE.Text = eventE.getStartTime();
			timeE = eventE.getStartTime().Split(':');
			hour_startE = int.Parse(timeE[0]);
			minute_startE = int.Parse(timeE[1]);


			btnStartDateE.Text = eventE.getStartDate();
			dateE = eventE.getStartDate().Split('/');
			year_startE = int.Parse(dateE[0]);
			month_startE = int.Parse(dateE[1]);
			day_startE = int.Parse(dateE[2]);

			spn_category.SetSelection(eventE.getCategoryID());
			spn_frequencyE.SetSelection(eventE.getFrequency());
			spn_pretimeE.SetSelection(eventE.getPretime());
		}

		//enable or disable the input controllers
		public void SetControls(Boolean value)
		{
			if (value == false)
			{
				btnEditE.Visibility = ViewStates.Visible;
				btnSaveE.Visibility = ViewStates.Invisible;
				btnResetE.Visibility = ViewStates.Invisible;
				btnRemoveE.Visibility = ViewStates.Visible;
			}
			else {
				btnEditE.Visibility = ViewStates.Invisible;
				btnSaveE.Visibility = ViewStates.Visible;
				btnResetE.Visibility = ViewStates.Visible;
				btnRemoveE.Visibility = ViewStates.Invisible;
			}

			txtTitleE.Enabled = value;
			txtDesE.Enabled = value;
			btnStartTimeE.Enabled = value;
			btnStartDateE.Enabled = value;
			spn_category.Enabled = value;
			spn_frequencyE.Enabled = value;
			spn_pretimeE.Enabled = value;
			voiceBtn.Enabled = value;
			voiceDescBtn.Enabled = value;

		}

		public void option_yes()
		{
			RemoveAlarmEvent();
			dbEventHelper.deleteEvent(int.Parse(evidDetail));
			Intent intent = new Intent(this, typeof(MainActivity));
			StartActivity(intent);
			this.Finish();
		}

		//remove event
		public void btnRemove()
		{
			//initiate dialog
			CustomDialog.Builder builder = new CustomDialog.Builder(this);
			builder.setTitle(GetString(Resource.String.confirm_text));
			builder.setMessage(GetString(Resource.String.delete_confirm_text));
			builder.setNegativeButton(GetString(Resource.String.cancel_text), delegate
			{
			});
			builder.setPositiveButton(GetString(Resource.String.yes_text), option_yes);
			builder.create(false, theme).Show();
		}

		//remove alarm in event
		public void RemoveAlarmEvent()
		{
			// Do nothing but close the dialog
			Intent intent_Alarm = new Intent(this, typeof(AlarmReceiver));
			PendingIntent pi = PendingIntent.GetBroadcast(this, int.Parse(eventE.getAlarmID()), intent_Alarm, PendingIntentFlags.CancelCurrent);
			am1 = (AlarmManager)GetSystemService(Context.AlarmService);
			am1.Cancel(pi);
		}

		public void prepareSave()
		{
			if (ifFieldsFilled() == false)
			{
				// do nothing
			}
			else {
				ifPretimeValid();
			}

		}

		public void save()
		{
			now = Calendar.GetInstance(tz);// get current time
										   //always be consistent with the phone time
			now.Set(CalendarField.Second, DateTime.Now.Second);
			now.Set(CalendarField.Minute, DateTime.Now.Minute);
			now.Set(CalendarField.HourOfDay, DateTime.Now.Hour);
			now.Set(CalendarField.DayOfMonth, DateTime.Now.Day);
			now.Set(CalendarField.Month, DateTime.Now.Month);
			now.Set(CalendarField.Year, DateTime.Now.Year);


			//set data into event (name,description etc.)
			eventE.setName(txtTitleE.Text);
			eventE.setDescription(txtDesE.Text);
			eventE.setStartDate(btnStartDateE.Text);
			eventE.setStartTime(btnStartTimeE.Text);
			eventE.setStatus(true);  //set to upcoming event
			eventE.setCategoryID((int)spn_category.SelectedItemPosition);
			eventE.setPretime((int)spn_pretimeE.SelectedItemPosition);
			eventE.setFrequency((int)spn_frequencyE.SelectedItemPosition);

			///Set the selected time
			selectedTimeE = Calendar.GetInstance(tz); //first initiate it so the second and millisecond is same with now
			selectedTimeE.Set(CalendarField.Year, year_startE);
			selectedTimeE.Set(CalendarField.Month, month_startE);
			selectedTimeE.Set(CalendarField.Date, day_startE);
			selectedTimeE.Set(CalendarField.HourOfDay, hour_startE);
			selectedTimeE.Set(CalendarField.Minute, minute_startE);
			selectedTimeE.Set(CalendarField.Second, 0);
			selectedTimeE.Set(CalendarField.Millisecond, 0);


			//command dataValidation
			if (DataValidation() == false)
			{
				//do nothing, because all toast is made inside datavalidation()
			}
			else {
				//get arguments we need
				//alarmStartTime is the first time the alarm rings

				//long alarmStartTime = selectedTimeE.TimeInMillis - daysOfPretimeE * 24 * 60 * 60 * 1000;
				//repeat time is how often the alarm rings
				//long alarmRepeatTime = hoursOfFrequencyE * 60 * 60 * 1000;
				//get arguments we need
				//alarmStartTime is the first time the alarm rings
				long alarmStartTime = selectedTimeE.TimeInMillis - daysOfPretimeE * 24 * 60 * 60 * 1000;
				//repeat time is how often the alarm rings
				long alarmRepeatTime = hoursOfFrequencyE * 60 * 60 * 1000;
				RemoveAlarmEvent();
				//if you want you test the repeat and pre alarm function, please apply the following section B
				//which will set the pre alarm time to 3 minutes which means you will be 
				//alarmed 3 minutesbefore the selected time
				//and the frequency will be set to 1 minute, so you will be reminded every minute from the first alarm to the last one
				/*--------------section B--------------------------
				long alarmStartTime = selectedTimeE.TimeInMillis - 3*60*1000;
				long alarmRepeatTime = 60 * 1000;
				----------------section B--------------------------*/
				Console.WriteLine("111alarmstarttime" + alarmStartTime.ToString());
				Console.WriteLine(selectedTimeE.TimeInMillis.ToString());

				//the time span between the selected time and the current time
				long spanInMilliseconds = alarmStartTime - now.TimeInMillis;
				Console.WriteLine("NOW" + now.TimeInMillis.ToString());
				Console.WriteLine(daysOfPretimeE.ToString() + " days");
				Console.WriteLine(alarmStartTime.ToString());
				Console.WriteLine(now.TimeInMillis.ToString());
				Console.WriteLine(((alarmStartTime - now.TimeInMillis) / 1000) + " seconds later");



				//set alarm which is repetitive;
				Intent intent_Alarm = new Intent(this, typeof(AlarmReceiver));
				intent_Alarm.PutExtra("alarmID", eventE.getAlarmID());
				intent_Alarm.PutExtra("eventName", eventE.getName());
				intent_Alarm.PutExtra("eventTime", getSelectedTime());
				intent_Alarm.PutExtra("eventTimeInMillis", selectedTimeE.TimeInMillis); //long
				intent_Alarm.PutExtra("eventFrequency", alarmRepeatTime); //long
				intent_Alarm.PutExtra("count", 0);
				PendingIntent pi = PendingIntent.GetBroadcast(this, int.Parse(eventE.getAlarmID()), intent_Alarm, PendingIntentFlags.UpdateCurrent);
				am1.SetExact(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + spanInMilliseconds, pi);


				//event and corresponding alarm will be calceled and removed to history event
				//update event to database
				dbEventHelper.UpdateEventByeventID(eventE);

				Toast.MakeText(this, GetString(Resource.String.savesuccessfully_text), ToastLength.Short).Show();

				btnEditE.Visibility = ViewStates.Visible;
				btnSaveE.Visibility = ViewStates.Invisible;
				btnResetE.Visibility = ViewStates.Invisible;
				SetControls(false);
			}
		}

		//check if necessary field is filled
		public Boolean ifFieldsFilled()
		{
			if (txtTitleE.Text.Equals(""))
			{  //if name has been filled which is necessary to be filled
				Toast.MakeText(this, GetString(Resource.String.forgetfields_text), ToastLength.Long).Show();
				return false;
			}
			//check if the time has been selected, which is necessary to fill
			else if (btnStartTimeE.Text.Equals(GetString(Resource.String.selecttime_text)) || btnStartDateE.Text.Equals(GetString(Resource.String.selectdate_text)))
			{//if user hasn't select a time at all
				Toast.MakeText(this, GetString(Resource.String.forgetdate_text), ToastLength.Long).Show();
				return false;
			}
			else return true;
		}

		public void ifPretimeValid()
		{
			daysOfPretimeE = 0;
			if (spn_pretimeE.SelectedItemId == 0) { daysOfPretimeE = 30; }
			else if (spn_pretimeE.SelectedItemId == 1) { daysOfPretimeE = 14; }
			else if (spn_pretimeE.SelectedItemId == 2) { daysOfPretimeE = 7; }
			else if (spn_pretimeE.SelectedItemId == 3) { daysOfPretimeE = 3; }
			else if (spn_pretimeE.SelectedItemId == 4) { daysOfPretimeE = 1; }
			else if (spn_pretimeE.SelectedItemId == 5) { daysOfPretimeE = 0; }


			hoursOfFrequencyE = 0;
			if (spn_frequencyE.SelectedItemId == 0) { hoursOfFrequencyE = 7 * 24; }
			else if (spn_frequencyE.SelectedItemId == 1) { hoursOfFrequencyE = 3 * 24; }
			else if (spn_frequencyE.SelectedItemId == 2) { hoursOfFrequencyE = 1 * 24; }
			else if (spn_frequencyE.SelectedItemId == 3) { hoursOfFrequencyE = 12; }

			//does the user want to check this, default option is yes if the frequency > pretime
			if (settings.GetString("PreFreCheck", "1") == "1")

			{//if yes
				if (daysOfPretimeE * 24 < hoursOfFrequencyE)
				{ //if it's a valid input
					CustomDialog.Builder builder = new CustomDialog.Builder(this);
					builder.setMessage(GetString(Resource.String.freq_long_text));
					builder.setTitle("eTime Reminder");
					builder.setPositiveButton(GetString(Resource.String.yes_text), delegate
					{
						if (builder.ifChecked() == true)
						{
							editor.PutString("PreFreCheck", "0"); // 0 means don't check again
							editor.Commit();
						}

						save();
					});

					builder.setNegativeButton(GetString(Resource.String.cancel_text), delegate
					{
					});

					builder.setCheckBox(GetString(Resource.String.remember_text));
					builder.create(false, theme).Show();//false means oncancelable = false
				}
				else {
					save();
				}
			}
			else {// no and not to ask them
				save(); //directly save
			}

		}



		//data validation method
		public Boolean DataValidation()
		{
			if (now.CompareTo(selectedTimeE) >= 0)
			{ //if the day set already past
				Console.WriteLine("111SYSTEM:D " + DateTime.Now.Hour);
				Console.WriteLine("111now: " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + " " + DateTime.Now.Day + "/" + DateTime.Now.Month + " " + DateTime.Now.Year);
				Console.WriteLine("111now: " + now.Get(CalendarField.HourOfDay) + ":" + now.Get(CalendarField.Minute) + " " + now.Get(CalendarField.DayOfMonth) + "/" + now.Get(CalendarField.Month) + " " + now.Get(CalendarField.Year));
				Console.WriteLine("111selected: " + selectedTimeE.Get(CalendarField.HourOfDay) + ":" + selectedTimeE.Get(CalendarField.Minute) + " " + selectedTimeE.Get(CalendarField.DayOfMonth) + "/" + selectedTimeE.Get(CalendarField.Month) + " " + selectedTimeE.Get(CalendarField.Year));
				Toast.MakeText(this, GetString(Resource.String.afternow_text), ToastLength.Long).Show();
				return false;
			}
			else if (pretimeCheck() == false)
			{ // if the days compatible with pretime setting
				return false;
			}
			else {

				return true;
			}
		}


		// check if select time is after now + pretime
		public Boolean pretimeCheck()
		{
			Calendar alarm_start_time = Calendar.GetInstance(tz); //get current time
			alarm_start_time.TimeInMillis = selectedTimeE.TimeInMillis - daysOfPretimeE * 24 * 60 * 60 * 1000;
			if (now.CompareTo(alarm_start_time) >= 0)
			{
				Toast.MakeText(this, GetString(Resource.String.after_pretime_check), ToastLength.Long).Show();
				return false;
			}
			else return true;
		}

		//to generate the selected time in a unique format
		// "Hour:Minute day/month/year"
		public String getSelectedTime()
		{
			String selectedString = (selectedTimeE.Get(CalendarField.HourOfDay) + ":" + selectedTimeE.Get(CalendarField.Minute)
					+ " " + selectedTimeE.Get(CalendarField.DayOfMonth) + "/" + selectedTimeE.Get(CalendarField.Month)
									 + "/" + selectedTimeE.Get(CalendarField.Year));
			return selectedString;
		}

		/**
     * Showing google speech input dialog for input name
     * */
		private void promptSpeechInput()
		{
			Intent intent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
			intent.PutExtra(RecognizerIntent.ExtraLanguageModel,
					RecognizerIntent.LanguageModelFreeForm);
			intent.PutExtra(RecognizerIntent.ExtraLanguage, Locale.Default);
			intent.PutExtra(RecognizerIntent.ExtraPrompt,
					GetText(Resource.String.speech_prompt));
			try
			{
				StartActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
			}
			catch (Android.Content.ActivityNotFoundException a)
			{
				Toast.MakeText(this,
						GetText(Resource.String.speech_not_supported),
						ToastLength.Short).Show();
			}
		}

		/**
     * Showing google speech input dialog for input description
     * */
		private void promptSpeechInputDescription()
		{
			Intent intent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
			intent.PutExtra(RecognizerIntent.ExtraLanguageModel,
							RecognizerIntent.LanguageModelFreeForm);
			intent.PutExtra(RecognizerIntent.ExtraLanguage, Locale.Default);
			intent.PutExtra(RecognizerIntent.ExtraPrompt,
					GetText(Resource.String.speech_prompt));
			try
			{
				StartActivityForResult(intent, REQ_CODE_SPEECH_DESCRIPTION);
			}
			catch (Android.Content.ActivityNotFoundException a)
			{
				Toast.MakeText(this,
						GetText(Resource.String.speech_not_supported),
						ToastLength.Short).Show();
			}
		}

		/**
     * Receiving speech input
     * */
		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			switch (requestCode)
			{
				case REQ_CODE_SPEECH_INPUT:
					{
						if (resultCode == Result.Ok && null != data)
						{

							IList<String> result = data
									.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
							txtTitleE.Text = result.ElementAt(0);
						}
						break;
					}
				case REQ_CODE_SPEECH_DESCRIPTION:
					{
						if (resultCode == Result.Ok && null != data)
						{

							IList<String> result = data
									.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
							txtDesE.Text = result.ElementAt(0);
						}
						break;
					}

			}
		}
	}
}

