﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Util;

namespace myApp2
{
	[Activity(Label = "AboutActivity", ParentActivity = typeof(MainActivity))]
	[MetaData("android.support.PARENT_ACTIVITY", Value = "com.companyname.myapp2.MainActivity")]
	public class AboutActivity : ActionBarActivity
	{
		TextView aboutPageTitle;
		TextView referenceText;
		ListView legalList;
		ImageView about_titleIcon;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			// Set to support only Portrait
			RequestedOrientation = ScreenOrientation.Portrait;
			// Get a SharedPreferences and its values
			ISharedPreferences preferences = GetSharedPreferences(MainActivity.PREFS_NAME, 0);
			int theme = preferences.GetInt("theme", 0);
			int language = preferences.GetInt("language", 0);
			Locale locale;
			string languageToLoad;

			// Set the language for the activity
			if (language == 0)
			{
				languageToLoad = "en";

			}
			else if (language == 1)
			{
				languageToLoad = "zh";
			}
			else if (language == 2)
			{
				languageToLoad = "vi";
			}
			else if (language == 3)
			{
				languageToLoad = "in";
			}
			else {
				languageToLoad = "en";
			}

			locale = new Locale(languageToLoad);
			Locale.Default = locale;
			Configuration config = new Configuration();
			config.Locale = locale;
			BaseContext.Resources.UpdateConfiguration(config,
					BaseContext.Resources.DisplayMetrics);
			SupportActionBar.Title = GetText(Resource.String.title_activity_about);






			SetContentView(Resource.Layout.activity_about);
			// Initalise widgets
			legalList = (ListView)FindViewById<ListView>(Resource.Id.legalList);
			referenceText = (TextView)FindViewById<TextView>(Resource.Id.reference_text);
			about_titleIcon = (ImageView)FindViewById<ImageView>(Resource.Id.about_titleIcon);

			if (theme == 0)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);
				about_titleIcon.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_main));
				about_titleIcon.SetPadding(0, 5, 0, 0);
			}
			else if (theme == 1)
			{
				about_titleIcon.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_natural));
				about_titleIcon.SetPadding(0, 5, 0, 0);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.natural_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.natural_background_color));
				aboutPageTitle.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				referenceText.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
			}
			else if (theme == 2)
			{
				about_titleIcon.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_dark));
				about_titleIcon.SetPadding(0, 5, 0, 0);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.dark_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.dark_background_color));
				aboutPageTitle.SetTextColor(Resources.GetColor(Resource.Color.white_color));
				referenceText.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
			}
			else {
				about_titleIcon.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_main));
				about_titleIcon.SetPadding(0, 5, 0, 0);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);
			}

			this.SupportActionBar.SetDisplayShowHomeEnabled(true); // Support a back button to previous Activity
			this.SupportActionBar.SetHomeButtonEnabled(true);

			//**************set the ListView***************
			// Create an array list
			List<String[]> list = new List<String[]>();


			// Create String Arrays then add to list
			String[] legalOne = new String[] { "default_music.mp3", "ccComunity", "Ben Warga" };
			list.Add(legalOne);

			String[] legalTwo = new String[] { "wake_up.mp3", "CC0", "Kevin MacLeod" };
			list.Add(legalTwo);

			String[] legalThree = new String[] { "exciting.mp3", "CC0", "Kevin MacLeod" };
			list.Add(legalThree);

			String[] legalFour = new String[] { "lyrical.mp3", "CC0", "Kevin MacLeod" };
			list.Add(legalFour);


			legalList.Adapter = new ReferenceAdapter(this, list); // Set the adapter for listView with adapter
			legalList.ItemClick += (object sender, AdapterView.ItemClickEventArgs args) => OnListItemClick(sender, args);

		}

		public void OnListItemClick(object sender, EventArgs e)
		{
			AdapterView.ItemClickEventArgs arg = (AdapterView.ItemClickEventArgs)e;
			int position = arg.Position;

			// Setting different actions on ListView
			if (position == 0)
			{
				Intent browserIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://freemusicarchive.org/music/Ben_Warga/Franklin_Canyon_Comp")); // Create an intent that opens browser and shows the reference.
				StartActivity(browserIntent); // Open browserIntent
			}
			else if (position == 1)
			{
				Intent browserIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://freepd.com/Chill/Anjou")); // Create an intent that opens browser and shows the reference.
				StartActivity(browserIntent); // Open browserIntent
			}
			else if (position == 2)
			{
				Intent browserIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://freepd.com/Electronic/Overt%20Intimidation%20Loop")); // Create an intent that opens browser and shows the reference.
				StartActivity(browserIntent); // Open browserIntent
			}
			else if (position == 3)
			{
				Intent browserIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("http://freepd.com/Acoustic/Morning%20Snowflake")); // Create an intent that opens browser and shows the reference.
				StartActivity(browserIntent); // Open browserIntent
			}
		}


	}
}

