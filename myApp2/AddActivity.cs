﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Speech;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.Util;
namespace myApp2
{
	[Activity(Label = "AddActivity", ParentActivity = typeof(MainActivity))]
	[MetaData("android.support.PARENT_ACTIVITY", Value = "com.companyname.myapp2.MainActivity")]
	public class AddActivity : ActionBarActivity
	{
		EventClass _event = new EventClass(); //this for create an event as an object
		DBEventHelper dbEventHelper; //create helper for event
		private AlarmManager am1 = null; // created the alarmManager
		private AlarmManager am2 = null; // created the alarmManager

		public static Java.Util.TimeZone tz = Java.Util.TimeZone.GetTimeZone("Australia/Sydney");
		Calendar now;
		Calendar selectedTime;

		//all start value will be -1 unless it's set by user
		int year_start, month_start, day_start;

		int hour_start, minute_start;

		//define buttons
		Button buttonSave;
		Button btnVoice;
		Button btnVoiceDescription;
		Button buttonReset;
		Button startTime;
		Button startDate;
		//define spinner
		Spinner spn_category;
		Spinner spn_pretime;
		Spinner spn_frequency;
		//define edit text
		EditText et_name; //this is for EditText_name
		EditText et_description; //this is for EditText_description
								 //textview
		TextView pageTitle;
		TextView pageName;
		TextView pageDescription;
		TextView pageTime;
		TextView pageCategory;
		TextView preTimeText;
		TextView frequencyText;

		TextView _dateDisplay;

		const int REQ_CODE_SPEECH_INPUT = 1;
		const int REQ_CODE_SPEECH_DESCRIPTION = 2;

		public int theme = 0;
		Locale locale;
		string languageToLoad;

		static int STARTDATE_DIALOG_ID = 0;
		static int STARTTIME_DIALOG_ID = 1;
		int daysOfPretime; //how nay days pf pretime setting
		int hoursOfFrequency;//how nay days pf frequency setting

		//get preference
		ISharedPreferences settings;
		ISharedPreferencesEditor editor;

		int alarmNum;//part of alarmIDr
		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait; //set to portrait

			// Get a SharedPreferences and its values
			ISharedPreferences preferences = GetSharedPreferences(MainActivity.PREFS_NAME, 0);
			theme = preferences.GetInt("theme", 0);
			int language = preferences.GetInt("language", 0);


			base.OnCreate(savedInstanceState);

			// Set the language for the activity
			if (language == 0)
			{
				languageToLoad = "en";

			}
			else if (language == 1)
			{
				languageToLoad = "zh";
			}
			else if (language == 2)
			{
				languageToLoad = "vi";
			}
			else if (language == 3)
			{
				languageToLoad = "in";
			}
			else {
				languageToLoad = "en";
			}

			locale = new Locale(languageToLoad);
			Locale.Default = locale;
			Configuration config = new Configuration();
			config.Locale = locale;
			BaseContext.Resources.UpdateConfiguration(config,
					BaseContext.Resources.DisplayMetrics);
			SupportActionBar.Title = GetText(Resource.String.title_activity_add);



			// Create your application here
			SetContentView(Resource.Layout.activity_add);
			// Initialise widgets
			et_name = (EditText)FindViewById<EditText>(Resource.Id.event_name);
			et_description = (EditText)FindViewById<EditText>(Resource.Id.event_description);
			spn_category = (Spinner)FindViewById<Spinner>(Resource.Id.spn_category);
			spn_frequency = (Spinner)FindViewById<Spinner>(Resource.Id.spnFrequency);
			spn_pretime = (Spinner)FindViewById<Spinner>(Resource.Id.spnPreTime);
			spn_pretime.SetSelection(5);
			spn_frequency.SetSelection(3);
			startTime = (Button)FindViewById(Resource.Id.start_time);
			startDate = (Button)FindViewById(Resource.Id.start_date);
			btnVoice = (Button)FindViewById(Resource.Id.btnVoice);
			btnVoiceDescription = (Button)FindViewById(Resource.Id.btnVoiceDescription);
			buttonReset = (Button)FindViewById(Resource.Id.reset_event);
			buttonSave = (Button)FindViewById(Resource.Id.save_event);
			pageName = (TextView)FindViewById(Resource.Id.addpage_name);
			pageDescription = (TextView)FindViewById(Resource.Id.addpage_description);
			pageTime = (TextView)FindViewById(Resource.Id.addpage_time);
			pageCategory = (TextView)FindViewById(Resource.Id.addpage_category);
			preTimeText = (TextView)FindViewById(Resource.Id.pre_text);
			frequencyText = (TextView)FindViewById(Resource.Id.frequency_text);

			// Set the theme to user's choice or default
			if (theme == 0)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);
			}
			else if (theme == 1)
			{

				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.natural_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.natural_background_color));
				pageTitle.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				pageName.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				pageDescription.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				pageTime.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				pageCategory.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				preTimeText.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				frequencyText.SetTextColor(Resources.GetColor(Resource.Color.natural_main_color));
				btnVoice.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				btnVoiceDescription.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				startDate.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				startTime.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				buttonReset.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));
				buttonSave.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaldesignbutton));


			}
			else if (theme == 2)
			{
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.dark_main_color)));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.dark_background_color));
				pageTitle.SetTextColor(Color.White);
				pageName.SetTextColor(Color.White);
				pageDescription.SetTextColor(Color.White);
				pageTime.SetTextColor(Color.White);
				pageCategory.SetTextColor(Color.White);
				preTimeText.SetTextColor(Color.White);
				frequencyText.SetTextColor(Color.White);
				btnVoice.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				btnVoiceDescription.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				startDate.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				startTime.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				buttonReset.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
				buttonSave.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkdesignbutton));
			}
			else {
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				Window.DecorView.SetBackgroundColor(Color.White);
			}


			//initialise shared preferences
			settings = GetSharedPreferences("my_settings", 0);
			editor = settings.Edit(); //for vibrator

			//initiate the alarmManager
			am1 = (AlarmManager)GetSystemService(Context.AlarmService);
			am2 = (AlarmManager)GetSystemService(Context.AlarmService);

			//Date dialog listener
			startDate.Click += delegate
			{
				DateOnClick();
			};
			//Time Dialog
			startTime.Click += (o, e) => ShowDialog(STARTTIME_DIALOG_ID);
			//reset listener
			buttonReset.Click += (o, e) => resetField();
			buttonSave.Click += (o, e) => btnSave();
			btnVoice.Click += delegate
			{
				promptSpeechInput();
			};
			btnVoiceDescription.Click += delegate
			{
				promptSpeechInputDescription();
			};

			//initialise the variables 
			now = Calendar.GetInstance(tz);
			selectedTime = now;
			dbEventHelper = new DBEventHelper(this);
			this.SupportActionBar.SetDisplayShowHomeEnabled(true); // Support a back button to previous Activity
			this.SupportActionBar.SetHomeButtonEnabled(true);
		}
		void ButtonClicked()
		{
			Toast.MakeText(this, GetString(Resource.String.forgetfields_text), ToastLength.Long).Show();
		}

		private void TimePickerCallback(object sender, TimePickerDialog.TimeSetEventArgs e)
		{
			hour_start = e.HourOfDay;
			minute_start = e.Minute;
			string time = string.Format("{0}:{1}", hour_start, minute_start.ToString().PadLeft(2, '0'));
			startTime.Text = time;
		}
		protected override Dialog OnCreateDialog(int id)
		{
			// Get the current time
			hour_start = DateTime.Now.Hour;
			minute_start = DateTime.Now.Minute;

			if (id == STARTTIME_DIALOG_ID)
				return new TimePickerDialog(this, TimePickerCallback, hour_start, minute_start, false);

			return null;
		}

		public void DateOnClick()
		{
			DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
			 {
				 year_start = time.Year;
				 month_start = time.Month;
				 day_start = time.Day;
				 startDate.Text = year_start + "/" + month_start + "/" + day_start;
			 });
			frag.Show(FragmentManager, DatePickerFragment.TAG);
		}

		public void resetField()
		{
			et_name.Text = "";
			et_description.Text = "";
			startTime.Text = GetString(Resource.String.selecttime_text);
			startDate.Text = GetString(Resource.String.selecttime_text);
			spn_category.SetSelection(0);

		}

		//when button save is clicked
		public void btnSave()
		{
			if (ifFieldsFilled() == false)
			{
				// do nothing
			}
			else {
				ifPretimeValid();
			}
		}

		//check if necessary field is filled
		public Boolean ifFieldsFilled()
		{
			if (et_name.Text.Equals(""))
			{  //if name has been filled which is necessary to be filled
				Toast.MakeText(this, GetString(Resource.String.forgetfields_text), ToastLength.Long).Show();
				return false;
			}
			//check if the time has been selected, which is necessary to fill
			else if (startTime.Text.Equals(GetString(Resource.String.selecttime_text)) || startDate.Text.Equals(GetString(Resource.String.selectdate_text)))
			{//if user hasn't select a time at all
				Toast.MakeText(this, GetString(Resource.String.forgetdate_text), ToastLength.Long).Show();
				return false;
			}
			else return true;
		}

		public void ifPretimeValid()
		{
			daysOfPretime = 0;
			if (spn_pretime.SelectedItemId == 0) { daysOfPretime = 30; }
			else if (spn_pretime.SelectedItemId == 1) { daysOfPretime = 14; }
			else if (spn_pretime.SelectedItemId == 2) { daysOfPretime = 7; }
			else if (spn_pretime.SelectedItemId == 3) { daysOfPretime = 3; }
			else if (spn_pretime.SelectedItemId == 4) { daysOfPretime = 1; }
			else if (spn_pretime.SelectedItemId == 5) { daysOfPretime = 0; }


			hoursOfFrequency = 0;
			if (spn_frequency.SelectedItemId == 0) { hoursOfFrequency = 7 * 24; }
			else if (spn_frequency.SelectedItemId == 1) { hoursOfFrequency = 3 * 24; }
			else if (spn_frequency.SelectedItemId == 2) { hoursOfFrequency = 1 * 24; }
			else if (spn_frequency.SelectedItemId == 3) { hoursOfFrequency = 12; }

			//does the user want to check this, default option is yes if the frequency > pretime
			if (settings.GetString("PreFreCheck", "1") == "1")

			{//if yes
				if (daysOfPretime * 24 < hoursOfFrequency)
				{ //if it's a valid input
					CustomDialog.Builder builder = new CustomDialog.Builder(this);
					builder.setMessage(GetString(Resource.String.freq_long_text));
					builder.setTitle("eTime Reminder");
					builder.setPositiveButton(GetString(Resource.String.yes_text), delegate
					{
						if (builder.ifChecked() == true)
						{
							editor.PutString("PreFreCheck", "0"); // 0 means don't check again
							editor.Commit();
						}

						save();
					});

					builder.setNegativeButton(GetString(Resource.String.cancel_text), delegate
					{
					});

					builder.setCheckBox(GetString(Resource.String.remember_text));
					builder.create(false, theme).Show();//false means oncancelable = false
				}
				else {
					save();
				}
			}
			else {// no and not to ask them
				save(); //directly save
			}

		}

		public void save()
		{
			now = Calendar.GetInstance(tz);// get current time
										   //always be consistent with the phone time
			now.Set(CalendarField.Second, DateTime.Now.Second);
			now.Set(CalendarField.Minute, DateTime.Now.Minute);
			now.Set(CalendarField.HourOfDay, DateTime.Now.Hour);
			now.Set(CalendarField.DayOfMonth, DateTime.Now.Day);
			now.Set(CalendarField.Month, DateTime.Now.Month);
			now.Set(CalendarField.Year, DateTime.Now.Year);

			//acquire the alarmID, which is generated according to the time
			//AlarmID will be used to cancel corresponding alarm
			String AlarmID = now.Get(CalendarField.HourOfDay) + now.Get(CalendarField.Minute)
								+ now.Get(CalendarField.Second) + getAlarmNum();

			//prevent lost of accuracy when alarmID is received by AlarmReceive when first digit is "0"

			AlarmID = long.Parse(AlarmID).ToString();//this is for keeping accuracy when passing data



			//set data into event (name,description etc.)
			_event.setName(et_name.Text);

			_event.setDescription(et_description.Text);

			_event.setStartDate(startDate.Text);

			_event.setStartTime(startTime.Text);

			_event.setStatus(true);  //set to upcoming event

			_event.setAlarmID(AlarmID);

			_event.setCategoryID((int)spn_category.SelectedItemPosition);
			_event.setPretime((int)spn_pretime.SelectedItemPosition);
			_event.setFrequency((int)spn_frequency.SelectedItemPosition);

			///Set the selected time
			selectedTime = Calendar.GetInstance(tz); //first initiate it so the second and millisecond is same with now
			selectedTime.Set(CalendarField.Year, year_start);
			selectedTime.Set(CalendarField.Month, month_start);
			selectedTime.Set(CalendarField.Date, day_start);
			selectedTime.Set(CalendarField.HourOfDay, hour_start);
			selectedTime.Set(CalendarField.Minute, minute_start);
			selectedTime.Set(CalendarField.Second, 0);
			selectedTime.Set(CalendarField.Millisecond, 0);


			//command dataValidation
			if (DataValidation() == false)
			{
				//do nothing, because all toast is made inside datavalidation()
			}
			else {
				//get arguments we need
				//alarmStartTime is the first time the alarm rings
				long alarmStartTime = selectedTime.TimeInMillis - daysOfPretime * 24 * 60 * 60 * 1000;
				//repeat time is how often the alarm rings
				long alarmRepeatTime = hoursOfFrequency * 60 * 60 * 1000;


				//if you want you test the repeat and pre alarm function, please apply the following section B
				//which will set the pre alarm time to 3 minutes which means you will be 
				//alarmed 3 minutesbefore the selected time
				//and the frequency will be set to 1 minute, so you will be reminded every minute from the first alarm to the last one
				/*--------------section B--------------------------
				long alarmStartTime = selectedTime.TimeInMillis - 3*60*1000;
				long alarmRepeatTime = 60 * 1000;
				----------------section B--------------------------*/
				Console.WriteLine("111alarmstarttime" + alarmStartTime.ToString());
				Console.WriteLine(selectedTime.TimeInMillis.ToString());

				//the time span between the selected time and the current time
				long spanInMilliseconds = alarmStartTime - now.TimeInMillis;
				Console.WriteLine("NOW" + now.TimeInMillis.ToString());

				Console.WriteLine(daysOfPretime.ToString() + " days");
				Console.WriteLine(alarmStartTime.ToString());
				Console.WriteLine(now.TimeInMillis.ToString());
				Console.WriteLine(((alarmStartTime - now.TimeInMillis) / 1000) + " seconds later");



				//set alarm which is repetitive;
				Intent intent_Alarm = new Intent(this, typeof(AlarmReceiver));
				intent_Alarm.PutExtra("alarmID", AlarmID);
				intent_Alarm.PutExtra("eventName", et_name.Text);
				intent_Alarm.PutExtra("eventTime", getSelectedTime());
				intent_Alarm.PutExtra("eventTimeInMillis", selectedTime.TimeInMillis); //long
				intent_Alarm.PutExtra("eventFrequency", alarmRepeatTime); //long
				intent_Alarm.PutExtra("count", 0);
				PendingIntent pi = PendingIntent.GetBroadcast(this, int.Parse(AlarmID), intent_Alarm, PendingIntentFlags.UpdateCurrent);
				am1.SetExact(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + spanInMilliseconds, pi);


				//add event to database
				dbEventHelper.addEvent(_event);// add the event to database
				alarmNum++;//record that one alarm is added in preference
						   //change preference of alarmNum
				editor.PutString("alarmNum", alarmNum.ToString());
				editor.Commit();
				Toast.MakeText(this, GetString(Resource.String.savesuccessfully_text), ToastLength.Short).Show();
				Toast.MakeText(this, GetString(Resource.String.alarmset_text), ToastLength.Long).Show();

				//reset everything
				resetField();
			}
		}

		//get alarmNum which is saved in preference, this will be part of alarmID
		public String getAlarmNum()
		{
			alarmNum = int.Parse(settings.GetString("alarmNum", "0"));
			return alarmNum.ToString();
		}

		//data validation method
		public Boolean DataValidation()
		{
			if (now.CompareTo(selectedTime) >= 0)
			{ //if the day set already past
				Console.WriteLine("111SYSTEM:D " + DateTime.Now.Hour);
				Console.WriteLine("111now: " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + " " + DateTime.Now.Day + "/" + DateTime.Now.Month + " " + DateTime.Now.Year);
				Console.WriteLine("111now: " + now.Get(CalendarField.HourOfDay) + ":" + now.Get(CalendarField.Minute) + " " + now.Get(CalendarField.DayOfMonth) + "/" + now.Get(CalendarField.Month) + " " + now.Get(CalendarField.Year));
				Console.WriteLine("111selected: " + selectedTime.Get(CalendarField.HourOfDay) + ":" + selectedTime.Get(CalendarField.Minute) + " " + selectedTime.Get(CalendarField.DayOfMonth) + "/" + selectedTime.Get(CalendarField.Month) + " " + selectedTime.Get(CalendarField.Year));
				Toast.MakeText(this, GetString(Resource.String.afternow_text), ToastLength.Long).Show();
				return false;
			}
			else if (pretimeCheck() == false)
			{ // if the days compatible with pretime setting
				return false;
			}
			else {

				return true;
			}
		}
		// check if select time is after now + pretime
		public Boolean pretimeCheck()
		{
			Calendar alarm_start_time = Calendar.GetInstance(tz); //get current time
			alarm_start_time.TimeInMillis = selectedTime.TimeInMillis - daysOfPretime * 24 * 60 * 60 * 1000;
			if (now.CompareTo(alarm_start_time) >= 0)
			{
				Toast.MakeText(this, GetString(Resource.String.after_pretime_check), ToastLength.Long).Show();
				return false;
			}
			else return true;
		}

		//to generate the selected time in a unique format
		// "Hour:Minute day/month/year"
		public String getSelectedTime()
		{
			String selectedString = (selectedTime.Get(CalendarField.HourOfDay) + ":" + selectedTime.Get(CalendarField.Minute)
					+ " " + selectedTime.Get(CalendarField.DayOfMonth) + "/" + selectedTime.Get(CalendarField.Month)
									 + "/" + selectedTime.Get(CalendarField.Year));
			return selectedString;
		}

		/**
     * Showing google speech input dialog for input name
     * */
		private void promptSpeechInput()
		{
			Intent intent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
			intent.PutExtra(RecognizerIntent.ExtraLanguageModel,
					RecognizerIntent.LanguageModelFreeForm);
			intent.PutExtra(RecognizerIntent.ExtraLanguage, Locale.Default);
			intent.PutExtra(RecognizerIntent.ExtraPrompt,
					GetText(Resource.String.speech_prompt));
			try
			{
				StartActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
			}
			catch (Android.Content.ActivityNotFoundException a)
			{
				Toast.MakeText(this,
						GetText(Resource.String.speech_not_supported),
						ToastLength.Short).Show();
			}
		}

		/**
     * Showing google speech input dialog for input description
     * */
		private void promptSpeechInputDescription()
		{
			Intent intent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
			intent.PutExtra(RecognizerIntent.ExtraLanguageModel,
							RecognizerIntent.LanguageModelFreeForm);
			intent.PutExtra(RecognizerIntent.ExtraLanguage, Locale.Default);
			intent.PutExtra(RecognizerIntent.ExtraPrompt,
					GetText(Resource.String.speech_prompt));
			try
			{
				StartActivityForResult(intent, REQ_CODE_SPEECH_DESCRIPTION);
			}
			catch (Android.Content.ActivityNotFoundException a)
			{
				Toast.MakeText(this,
						GetText(Resource.String.speech_not_supported),
						ToastLength.Short).Show();
			}
		}

		/**
     * Receiving speech input
     * */
		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			switch (requestCode)
			{
				case REQ_CODE_SPEECH_INPUT:
					{
						if (resultCode == Result.Ok && null != data)
						{

							IList<String> result = data
									.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
							et_name.Text = result.ElementAt(0);
						}
						break;
					}
				case REQ_CODE_SPEECH_DESCRIPTION:
					{
						if (resultCode == Result.Ok && null != data)
						{

							IList<String> result = data
									.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
							et_description.Text = result.ElementAt(0);
						}
						break;
					}

			}
		}
	}
}

